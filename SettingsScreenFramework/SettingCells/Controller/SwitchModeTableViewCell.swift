//
//  SwitchModeTableViewCell.swift
//  SettingsFramework
//
//  Created by SHIVANI DUBEY on 25/06/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

protocol ModeProtocol {
    func changeMode(isDark: Bool)
}

class SwitchModeTableViewCell: UITableViewCell {
    @IBOutlet weak var darkModeOn: UISwitch!
    var delegate: ModeProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func darkModeSwitch(_ sender: UISwitch) {
        delegate?.changeMode(isDark: sender.isOn)
    }
}
