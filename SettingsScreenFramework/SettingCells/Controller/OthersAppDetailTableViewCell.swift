//
//  OthersAppDetailTableViewCell.swift
//  SettingsFramework
//
//  Created by SHIVANI DUBEY on 25/06/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class OthersAppDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var appImage: UIImageView!
    @IBOutlet weak var appName: UILabel!
    @IBOutlet weak var appDesc: UILabel!
    @IBOutlet weak var othersHeight: NSLayoutConstraint!
    @IBOutlet weak var appHeight: NSLayoutConstraint!
    @IBOutlet weak var gapHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
