//
//  FooterTableViewCell.swift
//  SettingsFramework
//
//  Created by SHIVANI DUBEY on 25/06/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class FooterTableViewCell: UITableViewCell {
    @IBOutlet weak var linkWebsiteBtn: UIButton!
    @IBOutlet weak var copyrightTextLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        print(K.General.copyrightText)
        copyrightTextLbl?.text = K.General.copyrightText
        linkWebsiteBtn?.layer.cornerRadius = 29
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func linkWebsiteClicked(_ sender: Any) {
        if let url = URL(string: K.General.websiteLink) {
            UIApplication.shared.open(url)
        }
    }
    
}
