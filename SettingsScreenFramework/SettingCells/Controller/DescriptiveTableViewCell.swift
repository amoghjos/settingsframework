//
//  DescriptiveTableViewCell.swift
//  SettingsFramework
//
//  Created by SHIVANI DUBEY on 25/06/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

class DescriptiveTableViewCell: UITableViewCell {
    @IBOutlet weak var copyImage: UIImageView!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var titleValue: UILabel!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var copied: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func copyButton(_ sender: Any) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = titleValue.text
        if pasteboard.string != nil {
            UIView.transition(with: copied, duration: 2, options: .transitionCrossDissolve, animations: {
                self.copied.isHidden = false
            }) { (_) in
                UIView.transition(with: self.copied, duration: 2, options: .transitionCrossDissolve, animations: {
                    self.copied.isHidden = true
                })
            }
            UIView.transition(with: titleValue, duration: 1, options: .transitionCrossDissolve, animations: {
                self.titleValue.isHidden = true
            }) { (_) in
                UIView.transition(with: self.titleValue, duration: 1, options: .transitionCrossDissolve, animations: {
                    self.titleValue.isHidden = false
                })
            }
        }
    }
    
}
