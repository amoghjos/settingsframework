//
//  UserTableViewCell.swift
//  SettingsFramework
//
//  Created by SHIVANI DUBEY on 22/06/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

protocol UserProtocol {
    func openPhotos()
    func changeName(name: String)
}
class UserTableViewCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userType: UILabel!
    var delegate: UserProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userName.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func openPhotos(_ sender: Any) {
        delegate?.openPhotos()
    }
    //MARK: UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.changeName(name: textField.text ?? "")
    }
    
}
