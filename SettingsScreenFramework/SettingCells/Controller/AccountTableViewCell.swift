//
//  AccountTableViewCell.swift
//  SettingsFramework
//
//  Created by SHIVANI DUBEY on 25/06/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit

protocol AccountDetailProtocol {
    func changeEmailOrPhone(value: String)
}

class AccountTableViewCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var providerTypeImg: UIImageView!
    @IBOutlet weak var providerType: UILabel!
    @IBOutlet weak var provideAddress: UITextField!
    
    var accDelegate : AccountDetailProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        provideAddress.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: UITextFieldDelegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        accDelegate?.changeEmailOrPhone(value: textField.text ?? "")
    }

}
