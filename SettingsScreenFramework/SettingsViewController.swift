//
//  SettingsViewController.swift
//  SettingsFramework
//
//  Created by SHIVANI DUBEY on 22/06/20.
//  Copyright © 2020 SHIVANI DUBEY. All rights reserved.
//

import UIKit
import AVKit

public enum cellType {
    case userProfile
    case pro
    case common
    case switchMode
    case account
    case descriptive
    case otherAppsDetail
    case youTube
    case footer
}
enum commonCellTypes {
    case reminder
    case support
    case review
    case share
    case language
    case resetPassword
    case manageSubscription
    case signOut
    case delete
}
public protocol SettingsDelegate {
    func openReminder()
    func darkMode(mode: Bool)
    func setLanguage()
    func signOut()
    func deleteAccount()
    func changeName(value: String)
    func editEmail(value: String)
    func changePhone(phone: String)
    func editPhone(pin: String)
    func contactSupport()
    func shareSheet()
    func resetPassword()
    func openPhotos()
    func upgradePro()
    func goToAppStore()
}
public class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UserProtocol, AccountDetailProtocol, ModeProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonTop: NSLayoutConstraint!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeBtn: UIButton!
    var sections : Int = 14
    public var settingDelegate: SettingsDelegate?
    public var darkMode : Bool = false
    public var headerScrollable : Bool = false
    public var otherAppDetails : [[String : Any]] = []
    public var currentLanguage : String = ""
    var cellInSections : [Int] = [1, 1, 4, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1]
    var cellTypeArray : [cellType] = [.userProfile, .pro, .common, .switchMode, .common, .account, .common, .descriptive, .descriptive, .otherAppsDetail, .youTube, .common, .common, .footer]
    public var user: [String: Any] = [:]
    var commonType: [[commonCellTypes]] = [[.reminder, .support, .review, .share],[.language],[.resetPassword], [.signOut], [.delete]]
    var commonTypeIndex: [Int] = [-1, -1, 0, -1, 1, -1, 2, -2, -3, -1, -1, 3, 4, -1]
    public var closeButtonIsHidden : Bool = false
    var headerHeight : CGFloat = 90
    var minHeight : CGFloat = 30
    public init() {
        super.init(nibName: K.General.mainViewController, bundle: Bundle(for: SettingsViewController.self))
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        didRotateDevice()
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpView()
    }
    public override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           NotificationCenter.default.addObserver(self, selector: #selector(didRotateDevice), name: UIDevice.orientationDidChangeNotification, object: nil)
       }
    public override func viewDidDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
           NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
       }
    @objc func didRotateDevice() {
        if UIDevice.current.userInterfaceIdiom == .phone {
            if self.view.frame.width > self.view.frame.height {
                self.headerViewHeight.constant = 50
                self.headerHeight = 50
                self.buttonTop.constant = 12
                self.minHeight = 0
            } else {
                self.headerViewHeight.constant = 90
                self.headerHeight = 90
                self.buttonTop.constant = 52
                self.minHeight = 30
            }
        } else {
           self.headerViewHeight.constant = 90
            self.headerHeight = 90
            self.buttonTop.constant = 52
            self.minHeight = 30
        }
        self.view.layoutSubviews()
    }
    func setUpView() {
        let bundle = Bundle(for: type(of: self))
        tableView.register(UINib(nibName: K.General.userTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.userTableViewCell)
        tableView.register(UINib(nibName: K.General.youTubeTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.youTubeTableViewCell)
        tableView.register(UINib(nibName: K.General.footerTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.footerTableViewCell)
        tableView.register(UINib(nibName: K.General.commonTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.commonTableViewCell)
        tableView.register(UINib(nibName: K.General.proTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.proTableViewCell)
        tableView.register(UINib(nibName: K.General.switchModeTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.switchModeTableViewCell)
        tableView.register(UINib(nibName: K.General.accountTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.accountTableViewCell)
        tableView.register(UINib(nibName: K.General.descriptiveTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.descriptiveTableViewCell)
        tableView.register(UINib(nibName: K.General.othersAppDetailTableViewCell, bundle: bundle), forCellReuseIdentifier: K.General.othersAppDetailTableViewCell)
        tableView.rowHeight = UITableView.automaticDimension
        closeBtn.isHidden = closeButtonIsHidden
        setupTableRowAndSection()
    }
    public func setupTableViewConstraints(miniPlayer: Bool) {
        if miniPlayer {
            tableViewBottomConstraint.constant = 90
        } else {
            tableViewBottomConstraint.constant = 0
        }
    }
    func setupTableRowAndSection() {
        if let userType = user[K.General.userType] as? Bool, userType == false {
            if let provider = user[K.General.provider] as? String, provider.lowercased() == K.General.password {
                if otherAppDetails.count > 0 {
                    sections = 14
                    cellInSections = [1, 1, 4, 1, 1, 1, 1, 1, 1, otherAppDetails.count, 1, 1, 1, 1]
                    cellTypeArray = [.userProfile, .pro, .common, .switchMode, .common, .account, .common, .descriptive, .descriptive, .otherAppsDetail, .youTube, .common, .common, .footer]
                    commonType = [[.reminder, .support, .review, .share],[.language],[.resetPassword], [.signOut], [.delete]]
                    commonTypeIndex = [-1, -1, 0, -1, 1, -1, 2, -2, -3, -1, -1, 3, 4, -1]
                    
                } else {
                    sections = 13
                    cellInSections = [1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                    cellTypeArray = [.userProfile, .pro, .common, .switchMode, .common, .account, .common, .descriptive, .descriptive, .youTube, .common, .common, .footer]
                    commonType = [[.reminder, .support, .review, .share],[.language],[.resetPassword], [.signOut], [.delete]]
                    commonTypeIndex = [-1, -1, 0, -1, 1, -1, 2, -2, -3, -1, 3, 4, -1]
                }
            } else {
                if otherAppDetails.count > 0 {
                    sections = 13
                    cellInSections = [1, 1, 4, 1, 1, 1, 1, 1, otherAppDetails.count, 1, 1, 1, 1]
                    cellTypeArray = [.userProfile, .pro, .common, .switchMode, .common, .account, .descriptive, .descriptive, .otherAppsDetail, .youTube, .common, .common, .footer]
                    commonType = [[.reminder, .support, .review, .share],[.language], [.signOut], [.delete]]
                    commonTypeIndex = [-1, -1, 0, -1, 1, -1, -2, -3, -1, -1, 2, 3, -1]
                } else {
                    sections = 12
                    cellInSections = [1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                    cellTypeArray = [.userProfile, .pro, .common, .switchMode, .common, .account, .descriptive, .descriptive, .youTube, .common, .common, .footer]
                    commonType = [[.reminder, .support, .review, .share],[.language], [.signOut], [.delete]]
                    commonTypeIndex = [-1, -1, 0, -1, 1, -1, -2, -3, -1, 2, 3, -1]
                }
            }
        } else if let provider = user[K.General.provider] as? String, provider.lowercased() == K.General.password {
            if otherAppDetails.count > 0 {
                sections = 14
                cellInSections = [1, 4, 1, 1, 1, 1, 1, 1, otherAppDetails.count, 1, 1, 1, 1, 1]
                cellTypeArray = [.userProfile, .common, .switchMode, .common, .account, .common, .descriptive, .descriptive, .otherAppsDetail, .youTube, .common, .common, .common, .footer]
                commonType = [[.reminder, .support, .review, .share],[.language],[.resetPassword], [.manageSubscription], [.signOut], [.delete]]
                commonTypeIndex = [-1, 0, -1, 1, -1, 2, -2, -3, -1, -1, 3, 4, 5, -1]
            } else {
                sections = 13
                cellInSections = [1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                cellTypeArray = [.userProfile, .common, .switchMode, .common, .account, .common, .descriptive, .descriptive, .youTube, .common, .common, .common, .footer]
                commonType = [[.reminder, .support, .review, .share], [.language], [.resetPassword], [.manageSubscription], [.signOut], [.delete]]
                commonTypeIndex = [-1, 0, -1, 1, -1, 2, -2, -3, -1, 3, 4, 5, -1]
            }
        } else {
            if otherAppDetails.count > 0 {
                sections = 13
                cellInSections = [1, 4, 1, 1, 1, 1, 1, otherAppDetails.count, 1, 1, 1, 1, 1]
                cellTypeArray = [.userProfile, .common, .switchMode, .common, .account, .descriptive, .descriptive, .otherAppsDetail, .youTube, .common, .common, .common, .footer]
                commonType = [[.reminder, .support, .review, .share], [.language], [.manageSubscription], [.signOut], [.delete]]
                commonTypeIndex = [-1, 0, -1, 1, -1, -2, -3, -1, -1, 2, 3, 4, -1]
            } else {
                sections = 12
                cellInSections = [1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                cellTypeArray = [.userProfile, .common, .switchMode, .common, .account, .descriptive, .descriptive, .youTube, .common, .common, .common, .footer]
                commonType = [[.reminder, .support, .review, .share], [.language], [.manageSubscription], [.signOut], [.delete]]
                commonTypeIndex = [-1, 0, -1, 1, -1, -2, -3, -1, 2, 3, 4, -1]
            }
        }
        self.tableView.reloadData()
    }
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    //MARK: Table View Datasource, Delegate
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if headerScrollable {
            let offset = scrollView.contentOffset.y
            if offset > 0 {
                let y = self.headerHeight - (scrollView.contentOffset.y + self.headerHeight)
                let height = min(max(y, minHeight), (self.headerHeight + 10))
                print(offset, y, height)
                self.headerViewHeight.constant = height
            } else {
                let y = self.headerHeight - (scrollView.contentOffset.y + self.headerHeight)
                let height = max(min(y, minHeight), self.headerHeight)
                print(offset, y, height)
                self.headerViewHeight.constant = height
            }
        }
    }
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sections
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellInSections[section]
    }
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView()
        returnedView.backgroundColor = .clear
        return returnedView
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch cellTypeArray[indexPath.section] {
            case .youTube:
                guard (tableView.cellForRow(at: indexPath) as? YouTubeTableViewCell) != nil else {
                    return
            }
                guard let youtubeURL = URL(string: K.General.youTubeLink) else { return }
            UIApplication.shared.open(youtubeURL)
    
        case .otherAppsDetail:
            guard (tableView.cellForRow(at: indexPath) as? OthersAppDetailTableViewCell) != nil else {
                    return
            }
            if let appURLString = otherAppDetails[indexPath.row][K.General.appLink] as? String, let url = URL(string: appURLString) {
                UIApplication.shared.open(url)
            }
        case .common:
            guard (tableView.cellForRow(at: indexPath) as? CommonTableViewCell) != nil else {
                    return
            }
            let sect = commonTypeIndex[indexPath.section]
            let commonCellType = commonType[sect][indexPath.row]
            switch commonCellType {
                case .reminder:
                    settingDelegate?.openReminder()
                case .signOut:
                    let alert = UIAlertController(title: K.General.signOutAlert, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: K.General.yesAction, style: .destructive, handler: { _ in
                            self.dismiss(animated: true) {
                            self.settingDelegate?.signOut()
                        }
                    }))
                    alert.addAction(UIAlertAction(title: K.General.noAction, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                case .delete:
                    let alert = UIAlertController(title: K.General.deleteUserAlert, message: K.General.deleteUserMessage, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: K.General.yesAction, style: .destructive, handler: { _ in
                            self.dismiss(animated: true) {
                            self.settingDelegate?.deleteAccount()
                        }
                    }))
                    alert.addAction(UIAlertAction(title: K.General.noAction, style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                case .language:
                    settingDelegate?.setLanguage()
                case .share:
                    settingDelegate?.shareSheet()
                case .review:
                    writeReview()
                case .support:
                    settingDelegate?.contactSupport()
                case .resetPassword:
                    settingDelegate?.resetPassword()
                case .manageSubscription:
                    settingDelegate?.goToAppStore()
            }
            case .pro:
                settingDelegate?.upgradePro()
            default:
            print("clicked other function")
        }
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellTypeArray[indexPath.section] {
        case .userProfile:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.userTableViewCell, for: indexPath) as? UserTableViewCell else { return UITableViewCell() }
            return configureUserCell(cell)
        case .youTube:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.youTubeTableViewCell, for: indexPath) as? YouTubeTableViewCell else { return UITableViewCell() }
            cell.selectionStyle = .none
            return cell
        case .common:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.commonTableViewCell, for: indexPath) as? CommonTableViewCell else { return UITableViewCell() }
            let sect = commonTypeIndex[indexPath.section]
            return configureCommonCell(cell, commonCellType: commonType[sect][indexPath.row])
        case .footer:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.footerTableViewCell, for: indexPath) as? FooterTableViewCell else { return UITableViewCell() }
            cell.selectionStyle = .none
            return cell
        case .pro:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.proTableViewCell, for: indexPath) as? ProTableViewCell else { return UITableViewCell() }
            if let color = user[K.General.proColor] as? UIColor {
                cell.contentView.backgroundColor = color
            }
            cell.selectionStyle = .none
            return cell
        case .account:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.accountTableViewCell, for: indexPath) as? AccountTableViewCell else { return UITableViewCell() }
            return configureAccount(cell)
        case .switchMode:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.switchModeTableViewCell, for: indexPath) as? SwitchModeTableViewCell else { return UITableViewCell() }
            cell.darkModeOn.isOn = darkMode
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        case .descriptive:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.descriptiveTableViewCell, for: indexPath) as? DescriptiveTableViewCell else { return UITableViewCell() }
            return configureDescriptiveCell(cell, indexValue: commonTypeIndex[indexPath.section])
        case .otherAppsDetail:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: K.General.othersAppDetailTableViewCell, for: indexPath) as? OthersAppDetailTableViewCell else { return UITableViewCell() }
            return configureOtherApps(cell, otherAppDetail: otherAppDetails[indexPath.row], index: indexPath.row)
        }
    }
    func configureCommonCell(_ cell: CommonTableViewCell, commonCellType: commonCellTypes) -> CommonTableViewCell {
        let bundle = Bundle(for: type(of: self))
        switch commonCellType {
        case .reminder:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = false
            cell.typeName.text = K.General.reminder
            cell.typeImage.image = UIImage(named: K.General.reminder, in: bundle, compatibleWith: nil)
            cell.arrowImage.image = UIImage(named: K.General.sideArrow, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        case .support:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = false
            cell.typeName.text = K.General.support
            cell.typeImage.image = UIImage(named: K.General.support, in: bundle, compatibleWith: nil)
            cell.arrowImage.image = UIImage(named: K.General.sideArrow, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        case .review:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = false
            cell.typeName.text = K.General.review
            cell.typeImage.image = UIImage(named: K.General.review, in: bundle, compatibleWith: nil)
            cell.arrowImage.image = UIImage(named: K.General.sideArrow, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        case .share:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = false
            cell.typeName.text = K.General.share
            cell.typeImage.image = UIImage(named: K.General.share, in: bundle, compatibleWith: nil)
            cell.arrowImage.image = UIImage(named: K.General.sideArrow, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        case .language:
            cell.arrowText.isHidden = false
            cell.arrowImage.isHidden = false
            cell.typeName.text = K.General.language
            cell.arrowText.text = currentLanguage
            cell.typeImage.image = UIImage(named: K.General.language, in: bundle, compatibleWith: nil)
            cell.arrowImage.image = UIImage(named: K.General.dropdown, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        case .resetPassword:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = true
            cell.typeName.text = K.General.reset
            cell.typeImage.image = UIImage(named: K.General.reset, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        case .signOut:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = false
            cell.typeName.text = K.General.signOut
            cell.typeImage.image = UIImage(named: K.General.signOut, in: bundle, compatibleWith: nil)
            cell.arrowImage.image = UIImage(named: K.General.sideArrow, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        case .delete:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = false
            cell.typeName.text = K.General.deleteAccount
            cell.typeImage.image = UIImage(named: K.General.deleteAccount, in: bundle, compatibleWith: nil)
            cell.arrowImage.image = UIImage(named: K.General.deleteArrow, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.deleteAccount, in: bundle, compatibleWith: nil)
        case .manageSubscription:
            cell.arrowText.isHidden = true
            cell.arrowImage.isHidden = false
            cell.typeImage.image = UIImage(named: K.General.manageSubscription, in: bundle, compatibleWith: nil)
            cell.typeName.text = K.General.manageSubscription
            cell.arrowImage.image = UIImage(named: K.General.sideArrow, in: bundle, compatibleWith: nil)
            cell.typeName.textColor = UIColor(named: K.General.defaultColor, in: bundle, compatibleWith: nil)
        }
        cell.selectionStyle = .none
        return cell
    }
    func configureUserCell(_ cell: UserTableViewCell) -> UserTableViewCell {
        let bundle = Bundle(for: type(of: self))
        
        cell.userName.text =  user[K.General.userName] as? String ?? ""
        
        if let userType = user[K.General.userType] as? Bool, userType == true {
            cell.userType.text = K.General.paid
        } else {
            cell.userType.text = K.General.free
        }
        if let color = user[K.General.proColor] as? UIColor {
            cell.userType.textColor = color
        }
        cell.delegate = self
        if let img = user[K.General.imageURL] as? UIImage {
            cell.userImage.image = img
            self.profileImageCell(cell)
        } else {
            cell.userImage.image = UIImage(named: K.General.userImage, in: bundle, compatibleWith: nil)
        }
        cell.selectionStyle = .none
        return cell
    }
    func profileImageCell(_ cell: UserTableViewCell) {
        cell.userImage.layer.cornerRadius = (cell.userImage.frame.size.width) / 2
        cell.userImage.clipsToBounds = true
        cell.userImage.layer.borderWidth = 3.0
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                cell.userImage.layer.borderColor = UIColor.white.cgColor
            } else {
                cell.userImage.layer.borderColor = UIColor.black.cgColor
            }
        } else {
            cell.userImage.layer.borderColor = UIColor.black.cgColor
        }
        cell.selectionStyle = .none
    }
    func configureOtherApps(_ cell: OthersAppDetailTableViewCell, otherAppDetail: [String: Any], index: Int) -> OthersAppDetailTableViewCell {
        if index == 0 {
            cell.othersHeight.constant = 22
            cell.appHeight.constant = 20
            cell.gapHeight.constant = 20
        } else {
            cell.othersHeight.constant = 0
            cell.appHeight.constant = 0
            cell.gapHeight.constant = 10
        }
        cell.appImage.image = UIImage(named: otherAppDetail[K.General.appImg] as? String ?? "")
        cell.appName.text = otherAppDetail[K.General.appName] as? String ?? ""
        cell.appDesc.text = otherAppDetail[K.General.appDesc] as? String ?? ""
        cell.selectionStyle = .none
        return cell
    }
    func configureDescriptiveCell(_ cell: DescriptiveTableViewCell, indexValue: Int) -> DescriptiveTableViewCell {
        if indexValue == -2 { // UUID
            cell.copyImage.isHidden = false
            cell.titleName.text = K.General.uuid
            cell.titleValue.text = user[K.General.UserID] as? String ?? ""
            cell.copyButton.isHidden = false
        } else { //-3 -> App Version
            cell.copyImage.isHidden = true
            cell.titleName.text = K.General.appVersionValue
            cell.titleValue.text = user[K.General.appVersion] as? String ?? ""
            cell.copyButton.isHidden = true
        }
        cell.copied.isHidden = true
        cell.selectionStyle = .none
        return cell
    }
    func configureAccount(_ cell: AccountTableViewCell) -> AccountTableViewCell {
        let bundle = Bundle(for: type(of: self))
        if let provider = user[K.General.provider] as? String {
            print(provider)
            switch provider {
            case K.General.google:
                cell.providerType.text = K.General.createdUsing + K.General.googleImg
                cell.providerTypeImg.image = UIImage(named: K.General.googleImg, in: bundle, compatibleWith: nil)
                cell.provideAddress.text = user[K.General.email] as? String ?? ""
                cell.provideAddress.isEnabled = false
            case K.General.facebook:
                cell.providerType.text = K.General.createdUsing + K.General.fbImg
                cell.providerTypeImg.image = UIImage(named: K.General.fbImg, in: bundle, compatibleWith: nil)
                cell.provideAddress.text = user[K.General.email] as? String ?? ""
                cell.provideAddress.isEnabled = false
            case K.General.apple:
                cell.providerType.text = K.General.createdUsing + K.General.appleImg
                cell.providerTypeImg.image = UIImage(named: K.General.appleImg, in: bundle, compatibleWith: nil)
                cell.provideAddress.text = user[K.General.email] as? String ?? ""
                cell.provideAddress.isEnabled = false
            case K.General.phone.lowercased():
                cell.providerType.text = K.General.createdUsing + K.General.phoneImg
                cell.providerTypeImg.image = UIImage(named: K.General.phoneImg, in: bundle, compatibleWith: nil)
                cell.provideAddress.text = user[K.General.phone] as? String ?? ""
                cell.accDelegate = self
                cell.provideAddress.isEnabled = true
                cell.provideAddress.placeholder = K.General.phonePlaceholder
            default:
                cell.providerType.text = K.General.createdUsing + K.General.email
                cell.providerTypeImg.image = UIImage(named: K.General.email, in: bundle, compatibleWith: nil)
                cell.provideAddress.text = user[K.General.email] as? String ?? ""
                cell.accDelegate = self
                cell.provideAddress.isEnabled = true
                cell.provideAddress.placeholder = K.General.emailPlaceholder
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    public func reloadData() {
        self.tableView.reloadData()
    }
    func writeReview() {
        if let appString = user[K.General.appLinkURL] as? String, let appURL = URL(string: appString) {
            var components = URLComponents(url: appURL, resolvingAgainstBaseURL: false)
            components?.queryItems = [
                URLQueryItem(name: K.General.action, value: K.General.writeReview)
            ]
            guard let writeReviewURL = components?.url else {
              return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                UIApplication.shared.open(writeReviewURL)
            }
        }
    }
    //MARK: User Protocol
    func openPhotos() {
        settingDelegate?.openPhotos()
    }
    func changeName(name: String) {
        settingDelegate?.changeName(value: name)
    }
    //MARK: AccountDetailProtocol
    func changeEmailOrPhone(value: String) {
        if let provider = user[K.General.provider] as? String {
            if provider == K.General.password {
                settingDelegate?.editEmail(value: value)
            } else {
                settingDelegate?.changePhone(phone: value)
            }
        }
    }
    
    //MARK: ModeProtocol
    func changeMode(isDark: Bool) {
        settingDelegate?.darkMode(mode: isDark)
    }

}
